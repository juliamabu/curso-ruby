require_relative '../calc.rb'

describe Calc do
  subject {@calc}
  before{@calc = Calc.new}

  describe "#sum" do
    it "should sum 2 numbres" do
      expect(subject).to respond_to(:sum).with(2).arguments
      expect(subject.sum(2,2)).to eq(4)
      expect(subject.sum(2,3)).to eq(5)
      expect(subject.sum(-2,3)).to eq(1)
      expect(subject.sum(0,3)).to eq(3)
      expect(subject.sum(0,0)).to eq(0)
      #expect(subject.sum(3,'a')).to eq('ERROR')
    end

  end

end
