#Contar las palabras de un fichero

lineas=0
palabras=0
File.open('fichero.txt', 'r') do |f1|
  while linea = f1.gets
    lineas=lineas + 1
    palabras = linea.split(" ").length + palabras
  end
end

puts "Lineas: #{lineas}  Palabras: #{palabras}"

#Mismo ejemplo pero con el inject

File.open('fichero.txt', 'r') do |f1|
  puts f1.inject(0){|s,linea|
    s + linea.split(" ").length
  }
end
